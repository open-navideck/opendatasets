<?php

/**
 * @file
 * Installation file for storing errors from services client.
 */

/**
 * Implements hook_schema().
 */
function services_client_error_schema() {
  $schema['services_client_error'] = array(
    'description' => 'Collects errors of data pushes',
    'fields' => array(
      'eid' => array(
        'description'  => 'Error id',
        'type'        => 'serial',
        'unsigned'    => TRUE,
        'not null'    => TRUE,
        'no export'   => TRUE,
      ),
      'created' => array(
        'description' => 'Error time',
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => 0,
        'unsigned'    => TRUE,
      ),
      'entity_type' => array(
        'description' => 'Processed entity type',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
        'default'     => '',
      ),
      'entity_id' => array(
        'description' => 'Entity Id',
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => 0,
        'unsigned'    => TRUE,
      ),
      'event' => array(
        'description' => 'Stores the hook name where failure occurred.',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
        'default'     => '',
      ),
      'error_code' => array(
        'description' => 'Error code',
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => 0,
        'unsigned'    => TRUE,
      ),
      'error_message' => array(
        'description' => 'Error message',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
        'default'     => '',
      ),
      'entity' => array(
        'description' => 'Stores data that failed to send',
        'type'        => 'text',
        'size'        => 'medium',
        'serialize'   => TRUE,
      ),
      'retries' => array(
        'description' => 'Number of retries',
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => 0,
        'unsigned'    => TRUE,
      ),
      'status' => array(
        'description' => 'Stores result of retry operation(s).',
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => 0,
        'unsigned'    => TRUE,
      ),
    ),
    'primary key' => array('eid'),
    'indexes' => array(
      'status' => array('status'),
    ),
  );

  $schema['services_client_error_log'] = array(
    'description' => 'Actions log for error',
    'fields' => array(
      'lid' => array(
        'description'  => 'Log id',
        'type'        => 'serial',
        'unsigned'    => TRUE,
        'not null'    => TRUE,
        'no export'   => TRUE,
      ),
      'eid' => array(
        'description' => 'Error id',
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => 0,
        'unsigned'    => TRUE,
      ),
      'created' => array(
        'description' => 'Log item time',
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => 0,
        'unsigned'    => TRUE,
      ),
      'message' => array(
        'description' => 'Log message.',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
        'default'     => '',
      ),
      'status_change' => array(
        'description' => 'New status code',
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => 0,
        'unsigned'    => TRUE,
      ),
      'uid' => array(
        'description' => 'User id reference',
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => 0,
        'unsigned'    => TRUE,
      ),
      'error_code' => array(
        'description' => 'Error code',
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => 0,
        'unsigned'    => TRUE,
      ),
      'error_message' => array(
        'description' => 'Error message',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
        'default'     => '',
      ),
      'entity' => array(
        'description' => 'Stores data that failed to send',
        'type'        => 'text',
        'size'        => 'medium',
        'serialize'   => TRUE,
      )
    ),
    'primary key' => array('lid'),
    'indexes' => array(
      'eid' => array('eid'),
    ),
  );

  return $schema;
}

/**
 * Adjust errors table structure to latest version.
 */
function services_client_error_update_7200() {
  $schema = services_client_error_schema();

  // Remove old columns
  db_drop_field('services_client_error', 'connection');
  db_drop_field('services_client_error', 'hook');
  db_drop_field('services_client_error', 'token');

  $entity_field = array(
    'description' => 'Stores data that failed to send',
    'type'        => 'text',
    'serialize'   => TRUE,
  );

  // Rename existing fields
  db_change_field('services_client_error', 'task', 'event', $schema['services_client_error']['fields']['event']);
  db_change_field('services_client_error', 'data', 'entity', $entity_field);

  db_change_field('services_client_error_log', 'data', 'entity', $entity_field);
}

/**
 * Change `entity` field to blob type to contain more data.
 */
function services_client_error_update_7201() {
  $entity_field = array(
    'description' => 'Stores data that failed to send',
    'type'        => 'text',
    'size'        => 'medium',
    'serialize'   => TRUE,
  );

  db_change_field('services_client_error', 'entity', 'entity', $entity_field);
  db_change_field('services_client_error_log', 'entity', 'entity', $entity_field);
}
