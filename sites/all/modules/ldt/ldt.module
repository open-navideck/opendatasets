<?php
/**
 * @file
 * Developer tools for working with Linked Data.
 *
 * @copyright Copyright(c) 2012 Christopher Skene
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at xtfer dot com
 */

/**
 * Include the API.
 */
include_once 'ldt.api.php';

/**
 * Define a default library.
 */
const LDT_DEFAULT_LIBRARY = 'easyrdf';

/**
 * Define a default cache table.
 */
const LDT_DEFAULT_CACHE = 'cache_ldt';

/**
 * Define a default data format.
 */
const LDT_DEFAULT_FORMAT = 'rdfxml';


/** CTOOLS PLUGINS **********************************************************/

/**
 * Implements hook_ctools_plugin_directory().
 *
 * Tell ctools where our plugins live.
 */
function ldt_ctools_plugin_directory($module, $plugin) {
  if ($module == 'ldt') {
    return 'lib/Drupal/ldt/Plugins/' . $plugin;
  }
}

/**
 * Implements hook_ctools_plugin_type().
 *
 * Inform CTools about our plugins
 */
function ldt_ctools_plugin_type() {
  $items = array();
  $items['library_wrapper'] = array(
    'cache' => FALSE,
  );

  return $items;
}

/**
 * Load a plugin.
 *
 * @param string $type
 *   The type of plugin to load. Possible values are 'parser'.
 * @param string $name
 *   Name of the plugin
 *
 * @return array
 *   An array of plugin information
 */
function ldt_load_plugin($type, $name) {
  ctools_include('plugins');
  return ctools_get_plugins('ldt', $type, $name);
}

/**
 * Load all plugins of a type.
 *
 * @param string $type
 *   The type of plugin to load. Possible values are 'parser'.
 *
 * @return array
 *   An array of plugin information
 */
function ldt_load_plugins($type) {
  ctools_include('plugins');
  return ctools_get_plugins('ldt', $type);
}

/** DRUPAL HOOKS ************************************************************/

/**
 * Implements hook_flush_caches().
 *
 * This tells Drupal's cache handling system the name of our caching
 * table, so expired items will be purged automatically and this table
 * also affected by the empty all caches function.
 */
function ldt_flush_caches() {
  return array('cache_ldt');
}

/** MODULE FUNCTIONS ********************************************************/

/**
 * Get all library wrappers.
 */
function ldt_get_library_wrappers() {
  return module_invoke_all('ldt_library_wrappers');
}

/**
 * Implements hook_ldt_library_wrapper().
 */
function ldt_ldt_library_wrappers() {
  $wrappers = array();

  $wrappers['easyrdf'] = array(
    'name' => 'EasyRdf',
    'class' => '\Drupal\ldt\Plugins\EasyRdf',
  );

  return $wrappers;
}

/**
 * Get all specified data formats.
 *
 * Invokes 'hook_ldt_data_formats'.
 *
 * @return array
 *   An array of data format information.
 */
function ldt_data_formats() {
  return module_invoke_all('ldt_data_formats');
}

/**
 * Load an individual data format specification.
 *
 * @param string $type
 *   Machine name of the type.
 *
 * @return array|bool
 *   An array of format information, or FALSE.
 */
function ldt_data_format($type) {
  $formats = ldt_data_formats();

  if (array_key_exists($type, $formats)) {
    return $formats[$type];
  }

  return FALSE;
}

/**
 * Implements hook_ldt_data_formats().
 */
function ldt_ldt_data_formats() {
  $formats = array();

  $formats['php'] = array(
    'name' => 'RDF/PHP',
    'uri' => 'http://n2.talis.com/wiki/RDF_PHP_Specification',
  );
  $formats['rdfxml'] = array(
    'name' => 'RDF/XML',
    'accept' => array('application/rdf+xml'),
    'uri' => 'http://www.w3.org/TR/rdf-syntax-grammar',
    'extensions' => array('rdf, xrdf'),
  );
  $formats['json'] = array(
    'name' => 'JSON',
    'accept' => array('application/json', 'text/json', 'application/rdf+json'),
    'uri' => 'http://n2.talis.com/wiki/RDF_JSON_Specification',
    'extensions' => array('json'),
  );
  $formats['json-triples'] = array(
    'name' => 'RDF/JSON Triples',
  );
  $formats['ntriples'] = array(
    'name' => 'N-Triples',
    'uri' => 'http://www.w3.org/TR/rdf-testcases/#ntriples',
    'accept' => array(
      'text/plain',
      'text/ntriples',
      'application/ntriples',
      'application/x-ntriples',
    ),
    'extensions' => array('nt'),
  );
  $formats['turtle'] = array(
    'name' => 'Turtle Terse RDF Triple Language',
    'uri' => 'http://www.dajobe.org/2004/01/turtle',
    'accept' => array(
      'text/turtle',
      'application/turtle',
      'application/x-turtle',
    ),
    'extensions' => array('turtle'),
  );
  $formats['n3'] = array(
    'name' => 'Notation3',
    'uri' => 'http://www.w3.org/2000/10/swap/grammar/n3#',
    'accept' => array(
      'text/n3',
      'text/rdf+n3',
    ),
  );
  $formats['rdfa'] = array(
    'name' => 'RDFa',
    'uri' => 'http://www.w3.org/TR/rdfa-core/',
    'accept' => array(
      'text/html',
      'application/xhtml+xml',
    ),
  );

  $formats['jsonld'] = array(
    'name' => 'JSON-LD',
    'accept' => array('application/json'),
  );

  return $formats;
}

/**
 * Log an HTTP error.
 *
 * @param string|int $status
 *   An HTTP status code, or 0.
 * @param string $message
 *   Some descriptive text to help the user work out what happened.
 */
function ldt_log_http_error($status, $message = '') {

  // Build an error message for logging.
  if (!empty($message)) {
    $error = $status . ' (' . $message . ')';
  }
  else {
    $error = $status;
  }

  // Do some logging... We do this twice, once in line with other notices so
  // we can see where it happens, and once as an error.
  watchdog('ldt', "Request failed with error: %error.", array(
    '%error' => $error,
  ), WATCHDOG_NOTICE);
  drupal_set_message("Request failed with error: " . $error . ".", 'error');
}

/**
 * Implements hook_help().
 */
function ldt_help($path, $arg) {
  $output = '';

  switch ($path) {
    case 'admin/help#ldt':
      // Display the introduction paragraph.
      $output .= '<p>' . t('Linked Data Tools is a developer API for working with Linked Data. It simplifies the fetching, caching and manipulation of linked data.') . '</p>';

      // Test out using the module if we're actually on the help page.
      if ($_GET['q'] == 'admin/help/ldt') {
        $output .= '<h3>' . t('LDT examples') . '</h3>';

        module_load_include('inc', 'ldt', 'inc/ldt.example');

        $output .= '<h4><a href="http://dbpedia.org/data/Generalife.rdf">http://dbpedia.org/data/Generalife.rdf</a> as <strong>RDF/XML</strong></h4>';
        $xml_resource = ldt_example_get_dbpedia_rdfxml();
        $output .= $xml_resource->asHtml();

        $output .= '<h4><a href="http://cas.awm.gov.au/item/P00355.011">http://cas.awm.gov.au/item/P00355.011</a> as <strong>RDFa</strong></h4>';
        $rdfa_resource = ldt_example_get_rdfa();
        $output .= $rdfa_resource->asHtml();

        $output .= '<h4><a href="http://www.bbc.co.uk/music/artists/5441c29d-3602-4898-b1a1-b77fa23b8e50.json">http://www.bbc.co.uk/music/artists/5441c29d-3602-4898-b1a1-b77fa23b8e50.json</a> as <strong>RDF/XML</strong></h4>';
        $json_resource = ldt_example_get_bbc_rdfxml();
        $output .= $json_resource->asHtml();
      }
  }

  return $output;
}

/**
 * Helper to pretty print vars for examples.
 *
 * @param mixed $input
 *   The input.
 *
 * @return mixed|string
 *   The output.
 */
function _ldt_pretty_print($input) {
  if (function_exists('kprint_r')) {
    return kprint_r($input, TRUE);
  }
  else {
    return '<pre>' . print_r($input) . '</pre>';
  }
}